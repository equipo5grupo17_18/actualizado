
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class Apartado extends JFrame {
      public Apartado(){
        marco();
    }
    public void marco(){
    this.setSize(350,350);
    this.setLocation(200,100);
    this.setTitle("Apartado");
    this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
    
    //Panel que se vallan utilizando en la forma
    JPanel panelMayor = new JPanel(new BorderLayout());
    
    JPanel panelNorte = new JPanel  ();
    JPanel panelCentro = new JPanel ();
    JPanel panelSur = new JPanel    ();
    JPanel panelEste = new JPanel   ();
    //JPanel panelOeste = new JPanel   ();
    //Layout Managers a utilizar
    
   FlowLayout fl1= new FlowLayout(FlowLayout.CENTER);
   panelNorte.setLayout(fl1);
   
   GridLayout gl1 = new GridLayout(5,3,18,18);
   panelCentro.setLayout(gl1);
   
   FlowLayout fl2 = new FlowLayout();
   panelSur.setLayout(fl2);
   
   GridLayout gl2 = new GridLayout(8,0);
   panelEste.setLayout(gl2);

   
   
 //  JLabel lblTitulo = new JLabel("Apartado");

   //Creamos componentes para panel centro
   JLabel lbl1 = new JLabel("Marca :");
   JTextField tf1 = new JTextField(25);
   JLabel lbl2 = new JLabel ("Modelo :");
   JTextField tf2 = new JTextField(25);
   JLabel lbl3 = new JLabel("No.Serie :");
   JTextField tf3 = new JTextField(25);
   //JLabel lbl4 = new JLabel("Compañia : ");
   //JTextField tf4 = new JTextField(18);
   JLabel lbl5 = new JLabel ("Precio :");
   JTextField tf5 = new JTextField(25);
   JLabel lbl6 = new JLabel("Fecha Apartado :");
   JTextField tf6 = new JTextField(25);
   //JLabel lbl7 = new JLabel("Pago de Apartado : ");
   //JTextField tf7 = new JTextField(18);
    //JLabel lbl8 = new JLabel("Precio Restante :");
   //JTextField tf8 = new JTextField(18);
   //JLabel lbl9 = new JLabel("Plazo de Apartado : ");
   //JTextField tf9 = new JTextField(18);
   
    //Componentes para panel Sur
  
        JButton bs1 = new JButton();
        bs1.setText("Salir");
   
   //Componentes para panel Norte
        JButton bn1 = new JButton();
        bn1.setText("Modificar");
        JButton bn2 = new JButton();
        bn2.setText("Eliminar"); 
        JButton bn3 = new JButton();
        bn3.setText("Cancelar");
   
   //Agregado de componentes a paneles
   //panelNorte.add(lblTitulo);
   
   panelCentro.add(lbl1);
   panelCentro.add(tf1);
   panelCentro.add(lbl2);
   panelCentro.add(tf2);
   panelCentro.add(lbl3);
   panelCentro.add(tf3);
   //panelCentro.add(lbl4);
   //panelCentro.add(tf4);
   panelCentro.add(lbl5);
   panelCentro.add(tf5);
   panelCentro.add(lbl6);
   panelCentro.add(tf6);
  // panelCentro.add(lbl7);
  // panelCentro.add(tf7);
  // panelCentro.add(lbl8);
   //panelCentro.add(tf8);
   //panelCentro.add(lbl9);
  // panelCentro.add(tf9);
   
    panelNorte.add(bn1);
    panelNorte.add(bn2);
    panelNorte.add(bn3);
    
    panelSur.add(bs1);
        
   // panelEste.add(be1);
   // panelEste.add(be2);      
   //Integrantes paneles a panel Mayor
   panelMayor.add(panelNorte, BorderLayout.NORTH);
   panelMayor.add(panelCentro, BorderLayout.CENTER);
   panelMayor.add(panelSur, BorderLayout.SOUTH);
   panelMayor.add(panelEste,BorderLayout.EAST);
  // panelMayor.add(panelEste,BorderLayout.WEST);
   
    //Asociar el panel mayor a la forma o ventana..
        
        this.add(panelMayor);
        
        // f.pack();
        
        this.setVisible(true); 
    }
       
}
