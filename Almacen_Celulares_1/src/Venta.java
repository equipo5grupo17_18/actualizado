
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class Venta extends JFrame{
    public Venta(){
        marco();
    }    
        public void marco(){
        this.setSize(300,300);
        this.setLocation(200,100);
        this.setTitle("Venta");
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
            
        //Panel que se vallan utilizando en la forma
    JPanel panelMayor = new JPanel(new BorderLayout());
    
    JPanel panelNorte = new JPanel  ();
    JPanel panelCentro = new JPanel ();
    JPanel panelSur = new JPanel    ();
    JPanel panelEste = new JPanel   ();
  
    
    //Layout Managers a utilizar
    
    
   FlowLayout fl1= new FlowLayout(FlowLayout.CENTER);
   panelNorte.setLayout(fl1);
   
   GridLayout gl1 = new GridLayout(5,3,18,18);
   panelCentro.setLayout(gl1);
   
   FlowLayout fl2 = new FlowLayout();
   panelSur.setLayout(fl2);
   
   GridLayout gl2 = new GridLayout(8,0);
   panelEste.setLayout(gl2);
    
   //Creamos componentes para panel centro
   JLabel lbl1 = new JLabel("No. Venta :");
   JTextField tf1 = new JTextField(25);
   
   JLabel lbl2 = new JLabel ("Marca :");
   String[] lista = { "LG", "Samsung","Motorola","Sony","Nokia","IPhone"};
   JComboBox cb1 = new JComboBox(lista);
   
   JLabel lbl3 = new JLabel("Modelo :");
   String[] lista2 = { "Modelo 1", "Modelo 2","Modelo 3","Modelo 4"};
   JComboBox cb2 = new JComboBox(lista2);
   
   //JTextField tf3 = new JTextField(25);
   JLabel lbl4 = new JLabel("Cantidad :");
   JTextField tf4 = new JTextField(25);
   
   
        //Componentes para Panel Sur..
        
        JButton bs1 = new JButton();
        JButton bs2 = new JButton();
        bs1.setText("Salir");
        bs2.setText("Eliminar");
        
        //Componentes para panel este
        JButton be1 = new JButton();
        be1.setText("Agregar");
        JButton be2 = new JButton();
        be2.setText("Modificar"); 
        JButton be3 = new JButton();
        be3.setText("Cancelar");
        
         //Agregado de componentes a paneles
        panelCentro.add(lbl1);
        panelCentro.add(tf1);
        panelCentro.add(lbl2);
        panelCentro.add(cb1);
        panelCentro.add(lbl3);
        panelCentro.add(cb2);
     //   panelCentro.add(tf3);
        panelCentro.add(lbl4);
        panelCentro.add(tf4);
     
        panelSur.add(bs1);
        panelSur.add(bs2);    
        
        panelEste.add(be1);
        panelEste.add(be2); 
        panelEste.add(be3);
        
         panelMayor.add(panelCentro, BorderLayout.CENTER);
         panelMayor.add(panelSur, BorderLayout.SOUTH);
         panelMayor.add(panelEste,BorderLayout.EAST);
         
         //Asociar el panel mayor a la forma o ventana..
        
        this.add(panelMayor);
        this.setVisible(true); 
        }
    
}
