
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;


public class RApartado extends JFrame {
     public RApartado(){
    marco();
    }
    
    public void marco(){
        //JFrame f = new JFrame();
        this.setSize(500,300);
        this.setLocation(200,100);
        this.setTitle("Reporte");
                
        JPanel panelmayor = new JPanel(new BorderLayout());
        
        JPanel panelnorte  = new JPanel();
        JPanel panelcentro = new JPanel();
        JPanel panelsur    = new JPanel();
        JPanel paneleste   = new JPanel();
        JPanel paneloeste  = new JPanel();
        
        FlowLayout fll1 = new FlowLayout(FlowLayout.CENTER); //Layout Manager
        panelnorte.setLayout(fll1);
        
        GridBagLayout gbl1 = new GridBagLayout();
        GridBagConstraints gbc1 = new GridBagConstraints();
        panelcentro.setLayout(gbl1);
        
        FlowLayout fll2 = new FlowLayout();
        panelsur.setLayout(fll2);
        
        GridLayout gl2 = new GridLayout(3,0);
        paneleste.setLayout(gl2);
        
        GridLayout gl3 = new GridLayout(4,0);
        paneloeste.setLayout(gl3);
        
        //panel norte
         JLabel lblTitulo = new JLabel("Reporte de Apartado");
         
        JButton bn1 = new JButton("Buscar");
        
        //panel oeste
        String [] marca = {"LG", "Samsung","Motorola","Sony","Nokia","IPhone"};
        String [] modelo = { "Modelo 1", "Modelo 2","Modelo 3","Modelo 4"};
            
        JLabel lb1 = new JLabel("Marca: ");
        JComboBox cb1 = new JComboBox(marca);
        JLabel lb2 = new JLabel("Modelo: ");
        JComboBox cb2 = new JComboBox(modelo);
        
        JTable tabla = new JTable();
        
        tabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"Marca", "Modelo", "Precio", "Piezas"},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Marca", "Modelo", "Precio", "Piezas"
            }
        ));

        //panel sur
        JButton bs1 = new JButton("Imprimir");
        JButton bs2 = new JButton("Cancelar");
                   
        //panel centro
        
        //se agregan cosas a los paneles
        panelnorte.add(lblTitulo);
        
        panelnorte.add(bn1);
        
        gbc1.gridx = 0;
        gbc1.gridy = 0;
        //gbc1.weithy = 1.0;
        gbc1.anchor = GridBagConstraints.WEST;
        
       
        paneloeste.add(lb1);
        paneloeste.add(cb1);
        paneloeste.add(lb2);
        paneloeste.add(cb2);

        panelcentro.add(tabla);
        
        panelsur.add(bs1);
        panelsur.add(bs2);
                
        panelmayor.add(panelnorte, BorderLayout.NORTH);
        panelmayor.add(panelcentro, BorderLayout.CENTER);
        panelmayor.add(paneleste, BorderLayout.EAST);
        panelmayor.add(panelsur, BorderLayout.SOUTH);
        panelmayor.add(paneloeste, BorderLayout.WEST);
        
        this.add(panelmayor);
        
        this.setVisible(true);
    }
}
