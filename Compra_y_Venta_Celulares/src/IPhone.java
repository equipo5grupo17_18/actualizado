
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class IPhone extends JFrame{
    public IPhone(){
        marco();
    }
    public void marco() {   
        //Creando ventana
   // JFrame f =new JFrame();
    this.setSize(500,500);
    this.setLocation(200,100);
    this.setTitle("IPhone");
    this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
    
     //Panel que se vallan utilizando en la forma
    JPanel panelMayor = new JPanel(new BorderLayout());
    
    JPanel panelNorte = new JPanel  ();
    JPanel panelCentro = new JPanel ();
    JPanel panelSur = new JPanel    ();
   
    
     //Layout Managers a utilizar
    
    
   FlowLayout fl1= new FlowLayout(FlowLayout.CENTER);
   panelNorte.setLayout(fl1);
   
   GridLayout gl1 = new GridLayout(5,2,30,30);
   panelCentro.setLayout(gl1);
   
   FlowLayout fl2 = new FlowLayout();
   panelSur.setLayout(fl2);
   
   
   JLabel lblTitulo = new JLabel("CARACTERISTICAS");
   
   //Creamos componentes para panel norte
   JLabel lbl1 = new JLabel("Modelo :");
   JTextField tf1 = new JTextField(18);
   JLabel lbl2 = new JLabel ("Marca :");
   JTextField tf2 = new JTextField(18);
   JLabel lbl3 = new JLabel("No. Serie :");
   JTextField tf3 = new JTextField(18);
   
   
   String[] lista = { "TELCEL", "MOVISTAR","IUSACELL","UNEFON","LIBERADO"};
   JLabel lbl5 = new JLabel("Compañia :");
   JComboBox cb1 = new JComboBox(lista);
   
   //Componentes para panel Sur
  
   JButton bs1 = new JButton();
   JButton bs2 = new JButton();
   bs1.setText("Aceptar");
   bs2.setText("Cancelar");
   //Agregado de componentes a paneles
   panelNorte.add(lblTitulo);
   
   panelCentro.add(lbl1);
   panelCentro.add(tf1);
   panelCentro.add(lbl2);
   panelCentro.add(tf2);
   panelCentro.add(lbl3);
   panelCentro.add(tf3);
   panelCentro.add(lbl5);
   panelCentro.add(cb1);
   
    panelSur.add(bs1);
    panelSur.add(bs2);
    //Integrantes paneles a panel Mayor
   panelMayor.add(panelNorte, BorderLayout.NORTH);
   panelMayor.add(panelCentro, BorderLayout.CENTER);
   panelMayor.add(panelSur, BorderLayout.SOUTH);
     //Asociar el panel mayor a la forma o ventana..
        this.add(panelMayor);
        // f.pack();
        this.setVisible(true);         
           
    }
    
}
